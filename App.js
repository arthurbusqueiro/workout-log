import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import HomeScene from './Screens/Home';
import RegisterScene from './Screens/Register';
import DetailScene from './Screens/Detail';
import { Router, Scene } from 'react-native-router-flux';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Router>
        <Scene key="root">            
          <Scene key="homescene" component={HomeScene} initial={true} hideNavBar={true}/>
          <Scene key="registerscene" component={RegisterScene} hideNavBar={true}/>
          <Scene key="detailscene" component={DetailScene} hideNavBar={true}/>
        </Scene>
      </Router> 
    );
  }
}
