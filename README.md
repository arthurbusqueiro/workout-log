# README #

Esse README serve para configuração e execução do App Workout Log

* Instale o NodeJS (https://nodejs.org/en/download/)
* Com um terminal, navegue até a pasta onde se encontra o projeto extraído
* Rode o comando 'npm install' para instalar as dependências do projeto
* Rode o comando 'npm install -g react-native-cli' para instalar o react-native global
* Para configurar o ambiente do Android SDK, acesse:
* https://facebook.github.io/react-native/docs/getting-started.html
* Após a configuração, conecte um dispositivo Android ou execute um Emulador
* Rode a aplicação com o comando 'react-native run-android'

### Who do I talk to? ###

* Arthur Busqueiro
* https://bitbucket.org/arthurbusqueiro/
* arthurbusqueiro@gmail.com