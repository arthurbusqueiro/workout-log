package com.workout_log;

import com.facebook.react.ReactActivity;
import com.zappi.ui.material.letter.icon.RNMaterialLetterIconPackage;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Workout_Log";
    }
}
