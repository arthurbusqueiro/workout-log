
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar, TouchableOpacity, Alert, ScrollView, TouchableWithoutFeedback, Image} from 'react-native';
import RNMaterialLetterIcon from 'react-native-material-letter-icon';
import Moment from 'moment';
import Swipeout from 'react-native-swipeout';
import { Actions } from 'react-native-router-flux';
import AwesomeAlert from 'react-native-awesome-alerts';


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
       showAlert: false,
       id: '',
       workoutlog: (this.props.workoutlog !== undefined ? this.props.workoutlog : [
        {
          day: '2017-07-21',
          workouts: [
            {
              id: 1,
              type: 'Corrida',
              time: 4954,
              create: '2017-07-21 17:35:35'
            },
            {
              id: 2,
              type: 'Natação',
              time: 2719,
              create: '2017-07-21 18:22:35'
            }
          ]
        },
        {
          day: '2017-07-20',
          workouts: [
            {
              id: 3,
              type: 'Futebol',
              time: 6632,
              create: '2017-07-20 09:48:00'
            },
            {
              id: 4,
              type: 'Vôlei',
              time: 6632,
              create: '2017-07-20 08:27:51'
            }
          ]
        },
      ]),
      lastid: 4
      };
  };
 
  showAlert = (id, date) => {
    this.setState({
      showAlert: true,
      id: id,
      date: date
    });
  };
 
  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  
  deleteWorkout(){
    var log = this.state.workoutlog;
    var day = log.indexOf(log.filter(x => x.day === this.state.date)[0]);
    var event = log[day].workouts.indexOf(log[day].workouts.filter(x => x.id === this.state.id)[0]);
    log[day].workouts.splice(event, 1);
    if (log[day].workouts.length === 0){
      log.splice(day,1)
    }
    this.hideAlert();
    }

    render() {
      var days = [];
      var total = 0.0;
      var totalTime = 0.0;
      Moment.locale('pt');
      this.state.workoutlog.sort((a,b) => {
        return ((new Date(b.day).getTime() - (new Date(a.day))))
      });
      for (let index = 0; index < this.state.workoutlog.length; index++) {
        const element = this.state.workoutlog[index];
        const dayworkouts = []; 
        for (let y = 0; y < element.workouts.length; y++) {
          const workout = element.workouts[y];
          const time = Number(workout.time);
          totalTime += time;
          var h = Math.floor(time / 3600);
          var m = Math.floor(time % 3600 / 60);
          var s = Math.floor(time % 3600 % 60);
          let swipeBtns = [{
            component:(
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'column',
                }}
              >
                <Image source={require('../assets/img/trash.png')} />
              </View>
            ),
            backgroundColor: '#d8d8d8',
            underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
            onPress: () => { this.showAlert(workout.id, element.day) }
          }];
          dayworkouts.push(
            <Swipeout 
              right={swipeBtns} 
              autoClose={true} 
              backgroundColor={'#d8d8d8'} 
              key={y}>
              <TouchableWithoutFeedback onPress={() => Actions.detailscene({
                text: workout.type,
                date: Moment(element.day).format('DD/MM/YY'),
                time: '' + workout.time,
                id: workout.id,
                workoutlog: this.state.workoutlog
              })}>
                <View style={styles.workout}>
                  <View style={styles.flexOne}>
                    <RNMaterialLetterIcon
                      size={30}
                      letterSize={15}
                      shapeColor={"#9e9e9e"}
                      shapeType={"circle"}
                      letter={workout.type}
                      />
                  </View>
                  <View style={styles.flexTwo}>
                    <Text>
                      {workout.type}
                    </Text>
                  </View>
                  <View style={styles.flexOne}>
                    <Text style={styles.time}>
                      {h}h {m}m {s}s
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback >
            </Swipeout>
          );
          total++;
        }
        var day =
          <View key = {index}>
            <View style={styles.workoutDay}>
              <Text style={styles.date}>
                {Moment(element.day).format('DD/MM/YY')}
              </Text>
            </View>
            {dayworkouts}
          </View>
        ;
        days.push(day);
      }
      var h = Math.floor(totalTime / 3600);
      var m = Math.floor(totalTime % 3600 / 60);
      var s = Math.floor(totalTime % 3600 % 60);
        return (
          <View style={styles.container}>
            <View style={styles.navtop}>
              <View style={styles.titleView}>
                <Text style={styles.title}>Workout Log</Text>
              </View>
              <View style={styles.button}>
                <TouchableOpacity style={styles.btn} 
                onPress={() => {Actions.registerscene({workoutlog: this.state.workoutlog, lastid: this.state.lastid})}}>
                  <Text style={styles.btnText}>Novo</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.body} >
              <ScrollView>
                {days}
              </ScrollView>
            </View>
            <View style={styles.footer}>
              <View style={styles.totalSide}>
                <Text style={styles.strong}>TOTAL</Text>
                <Text style={styles.total}>{total} Exercícios</Text>
              </View>
              <View style={styles.timeSide}>
                <Text style={styles.totalTime}>{h}h {m}m {s}s</Text>
              </View>
            </View>
            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title="Remover exercício"
              message="Deseja remover o exercício?"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showCancelButton={true}
              showConfirmButton={true}
              cancelText="Não"
              confirmText="Sim, remover!"
              confirmButtonColor="#DD6B55"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.deleteWorkout();
              }}
            />
          </View >
        );
    }        
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    navtop: {
      backgroundColor: '#d8d8d8',
      color: '#1a1a1a',
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: 5
    },
    titleView: {
      color: '#1a1a1a',
      fontWeight: 'bold',
      flex: 1,
      alignItems: 'center',
      fontSize: 18
    },
    title: {
      color: '#1a1a1a',
      fontSize: 18
    },
    button: {
      color: '#f7f7f7',
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'center'
    },
    btn: {
      alignItems: 'center',
      backgroundColor: '#9b9b9b',
      paddingVertical: 10,
      paddingHorizontal: 40,
      borderRadius: 5,
      fontSize: 18
    },
    btnText: {
      color: '#f7f7f7',
      fontWeight: 'bold'
    },
    body: {
      flex: 9,
      backgroundColor: '#f2f2f2'
    },
    workoutDay: {
      paddingVertical: 5
    },
    workout: {
      borderColor: '#a3a3a3',
      padding: 10,
      backgroundColor: '#ffffff',
      flex: 1,
      flexDirection: 'row',
      paddingVertical: 15,
      marginVertical: 1
    },
    case: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    flexTwo:{
      flex: 2,
      justifyContent: 'center',
      backgroundColor: 'transparent',
    },
    flexOne: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      flexDirection: 'column'
    },
    date: {
      paddingLeft: 10,
      paddingTop: 10,
      color: '#8d8d8d'
    },
    time: {
      color: '#2f2f2f'
    },
    footer: {
      flex: 1,
      flexDirection: 'row'
    },
    strong: {
      fontWeight: 'bold'
    },
    total: {
      color: '#8e8e8e'
    },
    totalTime: {
      color: '#626262',
      fontSize: 18
    },
    totalSide:{
      flex: 1,
      alignItems: 'flex-start',
      justifyContent: 'center',
      paddingLeft: 10
    },
    timeSide:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    scroll:{
      flex: 1,
      flexDirection: 'column'
    }
  });
  