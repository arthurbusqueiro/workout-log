
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar, TextInput, TouchableOpacity, Alert, ScrollView, Image } from 'react-native';
import RNMaterialLetterIcon from 'react-native-material-letter-icon';
import Moment from 'moment';
import Swipeout from 'react-native-swipeout';
import { Actions } from 'react-native-router-flux';
import {Hoshi } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            text: '', 
            date: '',
            time: '',
            timeFormated: '',
            isDateTimePickerVisible: false,
            isDateTimeVisible: false,
            workoutlog: this.props.workoutlog !== undefined ? this.props.workoutlog : '',
            lastid: this.props.lastid !== undefined ? this.props.lastid : ''
        };
    }
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    
     _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    
     _handleDatePicked = (date) => {
         Moment.locale('pt');        
         this.setState({date: Moment(date).format('DD/MM/YY')});
        this._hideDateTimePicker();
     };

    _showTimePicker = () => this.setState({ isTimePickerVisible: true });
    
     _hideTimePicker = () => this.setState({ isTimePickerVisible: false });
    
     _handleTimePicked = (time) => {
         var date = new Date(time);
         var h = date.getHours();
         var m = date.getMinutes();
         var s = date.getSeconds();
         Moment.locale('pt');        
         this.setState({timeFormated: Moment(time).format('HH:mm:ss'), time: ((h*3600) + (m*60) + s)});
        this._hideTimePicker();
     };

     createWorkout(){
        var log = this.state.workoutlog;
        var day = log.indexOf(log.filter(x => Moment(new Date(x.day)).format('DD/MM/YY') === this.state.date)[0]);
        if (day < 0){
            log.push({
                day: Moment(new Date(this.state.date)).format('YYYY-MM-DD'),
                workouts: [{
                    id: this.state.lastid+1,
                    type: this.state.text,
                    time: this.state.time,
                    create: Moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                }]
            });
        }else{
            log[day].workouts.push({
                id: this.state.lastid+1,
                type: this.state.text,
                time: this.state.time,
                create: (new Date()).toDateString()
            });
        }
        log.lastid = this.state.lastid+1;
        Actions.homescene({workoutlog: log})
     }

    render() {
        return (
          <ScrollView  style={styles.container} enabled={false}>
            <View style={styles.navtop}>
              <View style={styles.titleView}>
                <TouchableOpacity 
                    onPress={() => Actions.pop()}>
                    <Image style={styles.backbutton} 
                        source={require('../assets/img/arrow_left.png')}
                    />
                </TouchableOpacity >
                <Text style={styles.title}>Novo</Text>
              </View>
              <View style={styles.button}>
                <TouchableOpacity style={styles.btn} 
                onPress={() => {this.createWorkout()}}>
                  <Text style={styles.btnText}>Salvar</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.body} >
                <Hoshi 
                    style={styles.input}
                    label={'Nome do exercício'}
                    labelStyle={styles.label}
                    borderColor={'#9b9b9b'}
                    onChangeText={(text) => { this.setState({text: text}) }}
                    inputStyle={styles.inputStyle}
                />
                <Hoshi 
                    style={styles.input}
                    label={'Data'} 
                    labelStyle={styles.label}
                    borderColor={'#9b9b9b'}
                    onFocus={this._showDateTimePicker}
                    value={this.state.date}
                    inputStyle={styles.inputStyle}
                    />
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
                <Hoshi 
                    style={styles.input}
                    label={'Tempo'} 
                    labelStyle={styles.label}
                    borderColor={'#9b9b9b'}
                    onFocus={this._showTimePicker}
                    value={this.state.timeFormated}
                    inputStyle={styles.inputStyle}
                    />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideTimePicker}
                    mode={'time'}
                />
            </View>
          </ScrollView>
        );
    }        
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffffff'
    },
    navtop: {
        backgroundColor: '#d8d8d8',
        color: '#1a1a1a',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10
    },
    titleView: {
        color: '#1a1a1a',
        fontWeight: 'bold',
        flex: 1,
        alignItems: 'center',
        fontSize: 18,
        flexDirection: 'row'
    },
    title: {
        color: '#1a1a1a',
        fontSize: 18,
        flex:1,
        marginLeft: 20
    },
    button: {
        color: '#f7f7f7',
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    btn: {
        alignItems: 'center',
        backgroundColor: '#9b9b9b',
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 5,
        fontSize: 18
    },
    btnText: {
        color: '#f7f7f7',
        fontWeight: 'bold'
    },
    body: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        paddingHorizontal: 10
    },
    input: {
        marginTop: 4,
        borderBottomWidth:1,
        borderColor: '#9b9b9b',
    },
    label:{
        color: '#9b9b9b',
        fontSize: 16,
        fontWeight: 'normal'
    },
    inputStyle:{
        color: '#1a1a1a',
        fontWeight: 'normal'
    },
    backbutton:{
        flex: 1,
        width: 30,
        marginLeft: 20,
        height: 35
    }
  });
  