
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar, TextInput, TouchableOpacity, Alert, ScrollView, Image } from 'react-native';
import RNMaterialLetterIcon from 'react-native-material-letter-icon';
import Moment from 'moment';
import Swipeout from 'react-native-swipeout';
import { Actions } from 'react-native-router-flux';
import {Sae } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import DateTimePicker from 'react-native-modal-datetime-picker';
import AwesomeAlert from 'react-native-awesome-alerts';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            text: this.props.text !='' ? this.props.text : '' , 
            date: this.props.date !='' ? this.props.date : '' , 
            time: this.props.time !='' ? this.props.time : '' ,
            id: this.props.id != '' ? this.props.id : '' ,
            timeFormated: '',
            showAlert: false,
            workoutlog: this.props.workoutlog !== '' ? this.props.workoutlog : ''
        };
        const time = Number(this.state.time);
        var h = Math.floor(time / 3600);
        var m = Math.floor(time % 3600 / 60);
        var s = Math.floor(time % 3600 % 60);
        this.state.timeFormated = h+'h ' + m + 'm ' + s + 's' 
    }

    showAlert = () => {
        this.setState({
            showAlert: true
          });
      };
     
      hideAlert = () => {
        this.setState({
            showAlert: false
          });
      };
      
      deleteWorkout(){
          var log = this.state.workoutlog;
          var day = log.indexOf(log.filter(x => Moment(x.day).format('DD/MM/YY') === this.state.date)[0]);
          var event = log[day].workouts.indexOf(log[day].workouts.filter(x => x.id === this.state.id)[0]);
          log[day].workouts.splice(event, 1);
          if (log[day].workouts.length === 0){
              log.splice(day,1)
          }
          this.hideAlert();
          Actions.homescene({
            workoutlog: log
          });
        }


    render() {
        return (
          <ScrollView  style={styles.container} enabled={false}>
            <View style={styles.navtop}>
              <View style={styles.titleView}>
                <TouchableOpacity 
                    onPress={() => Actions.pop()}>
                    <Image style={styles.backbutton} 
                        source={require('../assets/img/arrow_left.png')}
                    />
                </TouchableOpacity >
                <Text style={styles.title}>Detalhe</Text>
              </View>
              <View style={styles.button}>
                <TouchableOpacity style={styles.btn} 
                onPress={() => {this.showAlert()}}>
                  <Text style={styles.btnText}>Excluir</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.body} >
                <View style={styles.cardView}>
                    <Text style={styles.label}>
                        Exercício
                    </Text>
                    <Text style={styles.name}>
                        {this.state.text}
                    </Text>
                </View >
                <View style={styles.cardView}>
                    <Text style={styles.label}>
                        Data
                    </Text>
                    <Text style={styles.name}>
                        {this.state.date}
                    </Text>
                </View >
                <View style={styles.lastCardView}>
                    <Text style={styles.label}>
                        tempo
                    </Text>
                    <Text style={styles.name}>
                        {this.state.timeFormated}
                    </Text>
                </View >
            </View>
            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title="Remover exercício"
              message="Deseja remover o exercício?"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showCancelButton={true}
              showConfirmButton={true}
              cancelText="Não"
              confirmText="Sim, remover!"
              confirmButtonColor="#DD6B55"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.deleteWorkout();
              }}
            />
          </ScrollView>
        );
    }        
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffffff'
    },
    navtop: {
        backgroundColor: '#d8d8d8',
        color: '#1a1a1a',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10
    },
    titleView: {
        color: '#1a1a1a',
        fontWeight: 'bold',
        flex: 1,
        alignItems: 'center',
        fontSize: 18,
        flexDirection: 'row'
    },
    title: {
        color: '#1a1a1a',
        fontSize: 18,
        flex: 1,
        marginLeft:20        
    },
    button: {
        color: '#f7f7f7',
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    btn: {
        alignItems: 'center',
        backgroundColor: '#9b9b9b',
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 5,
        fontSize: 18
    },
    btnText: {
        color: '#f7f7f7',
        fontWeight: 'bold'
    },
    body: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    cardView:{
        paddingVertical: 30,
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderColor: '#e0e0e0'
    },
    lastCardView:{
        paddingVertical: 30,
        paddingHorizontal: 20
    },
    label:{
        color: '#9b9b9b',
        fontSize: 14,
        fontWeight: 'normal'
    },
    name:{
        fontSize: 18,
        color: '#1a1a1a',
        fontWeight: 'normal'
    },
    backbutton:{
        marginLeft:20,
        width: 30,
        height: 35,
        flex: 1
    }
  });
  